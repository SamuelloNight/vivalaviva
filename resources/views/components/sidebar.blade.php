<aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
    <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
        <i class="fe fe-x"><span class="sr-only"></span></i>
    </a>
    <nav class="vertnav navbar navbar-light">
        <div class="w-100 mb-4 d-flex">
            <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="{{ route('adminDashboard') }}">
                <h5 class="mb-2">{{ env('APP_NAME') }}</h5>
            </a>
        </div>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item dropdown">
                <a href="#dashboard" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                    <i class="fe fe-home fe-16"></i>
                    <span class="ml-3 item-text">Дашбоард</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100" id="dashboard">
                    <li class="nav-item active">
                        <a class="nav-link pl-3" href="{{ route('adminDashboard') }}">
                            <span class="ml-1 item-text">Статистика</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item dropdown">
                <a href="#fortune-wheel" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                    <i class="fe fe-gift fe-16"></i>
                    <span class="ml-3 item-text">Колесо фортуны</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100" id="fortune-wheel">
                    <li class="nav-item active">
                        <a class="nav-link pl-3" href="{{ route('adminFortuneWheelCreate') }}">
                            <span class="ml-1 item-text">Добавить приз</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link pl-3" href="{{ route('adminFortuneWheelList') }}">
                            <span class="ml-1 item-text">Список призов</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link pl-3" href="{{ route('adminFortuneWheelCustomersHistory') }}">
                            <span class="ml-1 item-text">История покупателей</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item dropdown">
                <a href="#project-settings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                    <i class="fe fe-settings fe-16"></i>
                    <span class="ml-3 item-text">Настройки проекта</span>
                </a>
                <ul class="collapse list-unstyled pl-4 w-100" id="project-settings">
                    <li class="nav-item active">
                        <a class="nav-link pl-3" href="{{ route('adminProjectSettingsArtisanInitialization') }}">
                            <span class="ml-1 item-text">Запуск artisan</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</aside>

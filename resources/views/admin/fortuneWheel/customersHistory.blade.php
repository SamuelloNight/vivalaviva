@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12 mb-4 d-flex justify-content-end">
            <div class="btn-group" role="group" aria-label="Basic example">
                <a class="btn mb-2 btn-secondary" href="?filter=new">Новые</a>
                <a class="btn mb-2 btn-secondary" href="?filter=success">Принятые</a>
                <a class="btn mb-2 btn-secondary" href="?filter=duplicate">Дубликаты</a>
                <a class="btn mb-2 btn-secondary" href="?filter=refused">Отказынне</a>
            </div>
        </div>
        <div class="col-12 mb-4">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Таблица призов</strong>
                </div>
                <div class="card-body">
                    <table class="table table-borderless table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Превью</th>
                                <th>Приз</th>
                                <th>Имя клиента</th>
                                <th>Телефон клиента</th>
                                <th>Номер чека</th>
                                <th>Дата создания</th>
                                <th>Дата редактирования</th>
                                <th>Изменить статус</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($winners as $winner)
                                <tr data-winner-id="{{ $winner->getProductWinnerId() }}">
                                    <td>{{ $winner->getProductWinnerId() }}</td>
                                    <td>
                                        <div class="avatar avatar-md">
                                            <img src="{{ $winner->getProductWinnerProduct()->getProductPreview() }}" alt="..." class="avatar-img rounded-circle">
                                        </div>
                                    </td>
                                    <td>
                                        <a href="{{ route('adminFortuneWheelEdit', ['id' => $winner->getProductWinnerProduct()->getProductId()]) }}">
                                            {{ $winner->getProductWinnerProduct()->getProductName() }}
                                        </a>
                                    </td>
                                    <td>{{ $winner->getProductWinnerCustomerName() }}</td>
                                    <td>{{ $winner->getProductWinnerCustomerPhone() }}</td>
                                    <td>{{ $winner->getProductWinnerOrderId() }}</td>
                                    <td>{{ $winner->getProductWinnerCreatedAt() }}</td>
                                    <td>{{ $winner->getProductWinnerUpdatedAt() }}</td>
                                    <td>
                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#" data-winner-change-status="{{ $winner->getProductWinnerId() }}|new">Новая заявка</a>
                                            <a class="dropdown-item" href="#" data-winner-change-status="{{ $winner->getProductWinnerId() }}|success">Успешно реализовано</a>
                                            <a class="dropdown-item" href="#" data-winner-change-status="{{ $winner->getProductWinnerId() }}|duplicate">Дубликат</a>
                                            <a class="dropdown-item" href="#" data-winner-change-status="{{ $winner->getProductWinnerId() }}|refused">Отказано</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('[data-winner-change-status]').click(function(event) {
            event.preventDefault();

            const productId = $(this).attr('data-winner-change-status').split('|')[0];
            const productStatus = $(this).attr('data-winner-change-status').split('|')[1];
            const formData = new FormData();

            formData.append('product_id', productId);
            formData.append('product_status', productStatus);

            $.ajax({
                url: '{{ route('apiAdminFortuneWheelWinnerUpdateStatus') }}',
                data: formData,
                context: this,

                success(response) {
                    alert(response.message ?? 'Системная ошибка. Свяжитесь с разработчиком.');

                    if (response.status === true) {
                        $(`[data-winner-id="${productId}"]`).remove();
                    }
                },

                error(error) {
                    alert('| Системная ошибка. Свяжитесь с разработчиком.');
                }
            });
        });
    </script>
@endsection

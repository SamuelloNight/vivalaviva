@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Форма приза</strong>
                </div>
                <div class="card-body">
                    <form name="product-create">
                        <input type="hidden" name="product_id" value="{{ $product->getProductId() }}">
                        <div class="form-group mb-3">
                            <label for="productName">Название приза</label>
                            <input class="form-control" id="productName" type="text" name="name" placeholder="Введите название приза" required value="{{ $product->getProductName() }}">
                        </div>
                        <div class="form-group mb-3">
                            <label for="productDescription">Описание приза</label>
                            <textarea class="form-control" id="productDescription" rows="4" name="description" placeholder="Напишите описание приза" required>{{ $product->getProductDescription() }}</textarea>
                        </div>
                        <div class="form-group mb-1">
                            <div class="avatar avatar-xl">
                                <img src="{{ $product->getProductPreview() }}" alt="..." class="avatar-img rounded-circle">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="productPreview">Изображение приза</label>
                            <div class="custom-file">
                                <input class="custom-file-input" id="productPreview" type="file" name="preview" accept="image/jpeg,image/jpg,image/png">
                                <label class="custom-file-label" for="productPreview">Выбрать картинку</label>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="customFile">Шанс выподение приза</label>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">%</span>
                                </div>
                                <input class="form-control" type="number" name="chance" minlength="1" maxlength="100" aria-label="Amount (to the nearest dollar)" required value="{{ $product->getProductChance() }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn mb-2 btn-primary">Обновить данные приза</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('form[name="product-create"]').submit(function(event) {
            event.preventDefault();

            $.ajax({
                url: '{{ route('apiAdminFortuneWheelUpdate') }}',
                context: this,
                data: new FormData(this),

                success(response) {
                    alert(response.message);

                    if (response.status === true) {
                        window.location = '{{ route('adminFortuneWheelList') }}';
                    }
                }
            });
        });
    </script>
@endsection

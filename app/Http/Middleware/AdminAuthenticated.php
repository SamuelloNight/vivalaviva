<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!admin()->check()) {
            if ($request->isJson()) {
                return response()->json([
                    'status' => false,
                    'message' => 'Ваше действие не авторизовано, данный функционал доступен только для администрации.'
                ], Response::HTTP_FORBIDDEN);
            }

            return redirect()->route('adminAuthSignIn');
        }

        return $next($request);
    }
}

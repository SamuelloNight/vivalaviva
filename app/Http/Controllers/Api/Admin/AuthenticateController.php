<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\RegistrationToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthenticateController extends Controller
{
    /**
     * login method
     */
    public function signIn(Request $request): array
    {
        $client = Admin::whereEmail($request->post('email'))->first();

        if ($client && Hash::check($request->post('password'), $client->getClientPassword())) {
            admin()->login($client);

            return [
                'status' => true,
                'data' => admin()->user(),
                'message' => 'Регистрация прошла успешно.'
            ];
        }

        return [
            'status' => false,
            'data' => null,
            'message' => 'Неверный логин или пароль.'
        ];
    }

    /**
     * registration method
     */
    public function signUp(Request $request): array
    {
        $accessToken = RegistrationToken::whereToken($request->post('token'))->first();

        if ($accessToken) {
            $create = Admin::create([
                'email' => $request->post('email'),
                'name' => $request->post('name'),
                'password' => Hash::make($request->post('password')),
            ]);

            if ($create) {
                $accessToken->delete();

                admin()->login($create);

                return [
                    'status' => true,
                    'data' => $create,
                    'message' => 'Регистрация прошла успешно.'
                ];
            }

            return [
                'status' => false,
                'data' => null,
                'message' => 'Произошла системная ошибка. Попробуйте позднее.'
            ];
        }

        return [
            'status' => false,
            'data' => [
                'token' => $request->post('token')
            ],
            'message' => 'Ваш токен доступа является недействительным.'
        ];
    }
}

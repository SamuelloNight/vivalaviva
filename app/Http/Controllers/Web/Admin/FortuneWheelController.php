<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductWinner;
use Illuminate\Http\Request;

class FortuneWheelController extends Controller
{
    public function productCreate()
    {
        return view('admin.fortuneWheel.productCreate', [
            'title' => 'Добавить новый приз в колесо фортуны',
            'description' => 'Пожалуйста, будьте внимательны при заполнении формы.'
        ]);
    }

    public function productsList()
    {
        return view('admin.fortuneWheel.productList', [
            'title' => 'Список призов колеса фортуны',
            'description' => 'Показан весь ассортимент колеса фортуны. Здесь вы можете их редактировать или удалить.',
            'products' => Product::all(),
        ]);
    }

    public function productEdit(Request $request)
    {
        return view('admin.fortuneWheel.productEdit', [
            'title' => 'Редактирование приза в колесе фортуны',
            'description' => 'Пожалуйста, будьте внимательны при заполнении формы.',
            'product' => Product::find($request->route('id'))
        ]);
    }

    public function productCustomersHistory(Request $request)
    {
        $filter = $request->get('filter');
        $winners = $filter ? ProductWinner::whereStatus($filter)->get() : ProductWinner::all();

        return view('admin.fortuneWheel.customersHistory', [
            'title' => 'Редактирование приза в колесе фортуны',
            'description' => 'Пожалуйста, будьте внимательны при заполнении формы.',
            'winners' => $winners
        ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @method static create(array $array)
 * @method static whereEmail(array|string|null $post)
 * @method static find(mixed $getProductAdminId)
 * @property mixed password
 * @property mixed email
 * @property mixed name
 */
class Admin extends Authenticatable
{
    use HasFactory, SoftDeletes, Notifiable;

    protected $table = 'admins';

    protected $fillable = [
        'email',
        'name',
        'password',
    ];

    protected $hidden = [
        'password',
    ];

    public function getClientEmail()
    {
        return $this->email;
    }

    public function getClientName()
    {
        return $this->name;
    }

    public function getClientPassword()
    {
        return $this->password;
    }
}

<?php

use App\Http\Controllers\Web\Common\FrontEndController;
use Illuminate\Support\Facades\Route;

Route::get('/', [FrontEndController::class, 'welcome'])->name('welcome');
Route::get('fortune-wheel', [FrontEndController::class, 'fortuneWheel'])->name('fortuneWheel');

//Route::get('/v2', function() {
//    return view('common.fortuneWheel');
//});

<?php

use App\Http\Controllers\Web\Admin\AuthenticateController;
use App\Http\Controllers\Web\Admin\FortuneWheelController;
use App\Http\Controllers\Web\Admin\ProjectSettingsController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->group(function() {
    Route::middleware('admin')->group(function() {
        Route::get('/dashboard', function() {
            return redirect()->route('adminFortuneWheelList');
        })->name('adminDashboard');

        Route::prefix('fortune-wheel')->group(function() {
            Route::get('create', [FortuneWheelController::class, 'productCreate'])->name('adminFortuneWheelCreate');
            Route::get('list', [FortuneWheelController::class, 'productsList'])->name('adminFortuneWheelList');
            Route::get('edit/{id}', [FortuneWheelController::class, 'productEdit'])->name('adminFortuneWheelEdit');
        });

        Route::prefix('winners')->group(function() {
            Route::prefix('customers')->group(function() {
                Route::get('list', [FortuneWheelController::class, 'productCustomersHistory'])->name('adminFortuneWheelCustomersHistory');
            });
        });

        Route::prefix('project-settings')->group(function() {
            Route::get('artisan-initialization', [ProjectSettingsController::class, 'artisanCommandsInitialization'])->name('adminProjectSettingsArtisanInitialization');
        });
    });

    Route::prefix('authenticate')->group(function() {
        Route::get('sign-in', [AuthenticateController::class, 'signIn'])->name('adminAuthSignIn');
        Route::get('sign-up', [AuthenticateController::class, 'signUp'])->name('adminAuthSignUp');
        Route::get('sign-out', [AuthenticateController::class, 'signOut'])->name('adminAuthSignOut');
    });
});

<?php

use App\Http\Controllers\Api\Customer\FortuneWheelController;
use Illuminate\Support\Facades\Route;

Route::prefix('customer')->group(function() {
    Route::prefix('fortune-wheel')->group(function() {
        Route::get('scroll', [FortuneWheelController::class, 'scroll']);
    });
});
